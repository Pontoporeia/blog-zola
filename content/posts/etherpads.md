+++
title = "Des Pads que j'utilise"
date = 2022-11-27
+++

Voici une liste des pads que j'utilise à l'ERG:

[https://pads.erg.be/p/DN-pcollectif-edition-serveur](https://pads.erg.be/p/DN-pcollectif-edition-serveur)


[https://pads.erg.be/p/APS_pratiques](https://pads.erg.be/p/APS_pratiques)

[https://pads.erg.be/p/aps___oj](https://pads.erg.be/p/aps___oj)

[https://pad.happyngreen.fr/p/reflexion_pratique_situ%C3%A9e](https://pad.happyngreen.fr/p/reflexion_pratique_situ%C3%A9e)