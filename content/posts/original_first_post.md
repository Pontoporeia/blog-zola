+++
title = "The original first real post"
date = 2022-10-24
+++



## Hey Casse, how long have you been fucking Nath ?

#### **220209 - 233342 <-> 220227 - 214623**

Premier post dans cette espace qui sent encore le PHP/js frais. Ci-dessous, des liens de la semaine(à voir), des notes sur mes projets et peut-être mes avis sur certains choses.

---

### Last we off

--> Je vais faire des tests avec [Godot](https://godotengine.org/) pour les espaces d'expositions. Un des todo est de tester les différentes interfaces, soit desktop, mobile, et VR.

--> Il est possible de packager les trois méthode, pour que lorsqu'on produit les exécutables, il fasse les trois version en même temps. À voir.

--> travailler la méthode de recherche et l'hygiène de travail.

--> se donner le temps de réfléchir. Ne pas passer trop de temps sur l'ordinateur. FOMO is real !

--> todo list qui sont simple à completer en un temps cours.

--> méthodiquement mettre en place un calendrier organisé.


### Les liens de la semaine:

**Des aggrégateur de liens intéressents**:

https://lobste.rs/

https://news.ycombinator.com/

**Ensuite les highlights**:

[Before I go: When it comes to complaining about web browsers - daverupert.com](https://daverupert.com/2022/02/complaining-about-web-browsers/)

[How does UTF-8 turn “😂” into “F09F9882”?](https://sethmlarson.dev/blog/utf-8)

https://yourcalendricalfallacyis.com/

https://twitter.com/dnunan79/status/1491072159393148929

https://cacm.acm.org/magazines/2020/10/247600-madmax/fulltexthttps://cacm.acm.org/magazines/2020/10/247600-madmax/fulltext

[The Plausibly Deniable DataBase (PDDB)](https://www.bunniestudios.com/blog/?p=6307)

[Digital Advertising in 2022 &#8211; Stratechery by Ben Thompson](https://stratechery.com/2022/digital-advertising-in-2022/)
[Dylan Beattie "There's no such thing as Plain Text"](https://www.youtube.com/watch?v=oYd2KkuZLbE)

[EE380 Stanford Talk by David Rosenthal about Cryptocurrencies](https://blog.dshr.org/2022/02/ee380-talk.html)

**Et au final les ressources Libre Open-Source**:

[GitHub - gyroflow/gyroflow: Video stabilization using gyroscope data](https://github.com/gyroflow/gyroflow)

[TPAGM / Tinyblog · GitLab](https://gitlab.com/Zipperflunky/tinyblog)

[GitHub - RajSolai/TextSnatcher: How to Copy Text from Images ? Answer is TextSnatcher !. Perform OCR operations in seconds on Linux Desktop.](https://github.com/RajSolai/TextSnatcher)





