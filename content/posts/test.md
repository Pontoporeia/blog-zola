+++
title = "A Test Post from my old handmade blog"
date = 2022-10-24
+++

# Hello
### **220208 - 015605**

This is a test post. I always wonder how it's going to look but hey, il va bien falloir que je m'y fasse à écrire en anglais :D

---


*I feeling italic*

**This is a bold move**
- hey
- yes ?
- think

**Small code:** 

Testing testing one two one two `testing testing one two one two` I wonder how this is working because I need to write a lot so that the text goes under the code block to check that the margin is working properly :D

**Big Code:**

```python
# This is a test python code to try highlight.js
f = open("demofile.txt", "r")
for x in f:
  print(x)

```

[https://github.com/erusev/parsedown](https://github.com/erusev/parsedown)
