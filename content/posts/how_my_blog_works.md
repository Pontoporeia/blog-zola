+++
title = "How my blog works ?"
date = 2022-10-25
+++



Je me suis enfin créer un espace numérique textuelle dont je suis un peu fière et que je vais investir.
Deux spécificité du processus de construction de mon site dont je voudrais parler sont : 

- le css responsive en moins de 10 lignes de css
  
  - Pourquoi faire des sites de petites tailles numériques ?
    - 512kb

- le static site generator (SSG) Zola qui génère mon site entier

## HTML/CSS/JS: 58kb, 100kb et le 512kb club

À l’heure actuelle, selon [GTMetrix](https://gtmetrix.com), mon site fait **17.8Kb non-compressé** quand il arrive dans votre navigateur et prend **978ms** pour être complètement afficher à l’écrans.

Je passe beaucoup de temps sur [Hacker News](news.ycombinator.com). C’est autant une mine de ressources tout tech, web, programmation etc, mais aussi un vraie point de fuite de temps aussi. 

Mais que je suis tombé sur ce [gist](https://gist.github.com/JoeyBurzynski/617fb6201335779f8424ad9528b72c41), je peux dire avec certitude que c’est ce qui a finit par me pousser à reprendre la quête du blog/web space que [j’avais déjà tenté de faire en PHP](https://codeberg.org/Pontoporeia/tinyblog). 

Voici les 7 lignes de css: 

```css
html {
  max-width: 70ch;
  padding: 3em 1em;
  margin: auto;
  line-height: 1.75;
  font-size: 1.25em;
}
```

*C’est la version 100 bytes du CSS reponsive.*

Je dois vous avouer, j’ai rajouter :

```css
h1,h2,h3,h4,h5,h6 {
  margin: 3em 0 1em;
}

p,ul,ol {
  margin-bottom: 2em;
  color: #1d1d1d;
  font-family: sans-serif;
}
```

Avec ces **200 bytes de CSS**, je pouvais enfin faire un site rapidement qui soit responsive tout en étant très rapide à charger. 

L'intérêt d'essayer de réduire la taille d'un site est intiment lié à la vitesse d'internet, et combien de temps les visiteurs doivent attendre pour recevoir l'entièreté des fichiers de mon site pour voir la page s'afficher.

## Zola comme Static Site Generator: templates et moins de 50ms de temps de compilation

Depuis que j’ai découvert le site [solar.lowtechmagazine](https://solar.lowtechmagazine.com), je me suis mis en tête de faire un site statique, sans PHP et le minimum syndical de javascript.
Les Static Site Generators sont des systèmes qui permettent de généré de l’HTML et du JAVASCRIPT à partir de templates qui accueille les contenus eux écrit en Markdown. 

J’ai choisi Zola car je suis intrigué par le langage de programmation Rust, qui est vendu comme étant un compétiteur à des langages Bas Niveau comme C. 
Ce choix arbitraire s’est prouvé comme étant un superbe moyen de créer mon site perso. 

Zola n’est qu’un seul fichier binaire à installé sur son ordi. C'est un outil qui permet de générer un projet, de lancer une instance live locale, et de créer le site finale static à uploader dans un serveur web (Nginx, Apache, Caddy, ...). Je reviendrais sur comment j'ai mis en place ce serveur dans un post prochain.

Voici la structure de mon dossier de travail, qui est aussi un [repo sur Codeberg](https://codeberg.org/Pontoporeia/blog-zola) (généré avec `tree`)

```
├── config.toml
├── content
│   ├── cv
│   │   └── _index.md
│   ├── gallery
│   │   └── _index.md
│   ├── posts
│   │   ├── first.md
│   │   ├── _index.md
│   │   ├── original_first_post.md
│   │   └── test.md
│   ├── projects
│   │   └── _index.md
│   └── publications
│       └── _index.md
├── LICENSE
├── public
│   ├── 404.html
│   ├── atom.xml
│   ├── cv
│   │   └── index.html
│   ├── elasticlunr.min.js
│   ├── gallery
│   │   └── index.html
│   ├── icons
│   │   ├── by.svg
│   │   ├── cc.svg
│   │   ├── codeberg.svg
│   │   ├── favicon.svg
│   │   ├── mail.svg
│   │   ├── mastodon.svg
│   │   ├── matrix.svg
│   │   └── rss.svg
│   ├── img
│   ├── index.html
│   ├── posts
│   │   ├── first
│   │   │   └── index.html
│   │   ├── index.html
│   │   ├── original-first-post
│   │   │   └── index.html
│   │   └── test
│   │       └── index.html
│   ├── projects
│   │   └── index.html
│   ├── publications
│   │   └── index.html
│   ├── robots.txt
│   ├── search_index.en.js
│   ├── sitemap.xml
│   └── style.css
├── README.md
├── static
│   ├── icons
│   │   ├── by.svg
│   │   ├── cc.svg
│   │   ├── codeberg.svg
│   │   ├── favicon.svg
│   │   ├── mail.svg
│   │   ├── mastodon.svg
│   │   ├── matrix.svg
│   │   └── rss.svg
│   ├── img
│   └── style.css
└── templates
    ├── 404.html
    ├── base.html
    ├── index.html
    ├── nav.html
    ├── page.html
    ├── robot.txt
    └── section.html
```

Tous les paramétrages sont dans le fichier `config.toml`. 
Les templates sont en .html et se trouve dans le dossier du même nom. 
Les fichier static, comme le CSS et les images sont dans le dossier `static` et le contenue est sous `content`, départager par section dans chaque dossier avec `_index.md` qui décrit chaque section et sert de page accueil.

Lors du développement du site, je peux voir en temps réel les changements grâce à la commande `zola serve`, lancer depuis la tête du projet.

Après la première configuration et à chaque nouvelle pages, il me suffit de taper `zola build` pour lancer la compilation et génération du site static dans le dossier `public`.

Dans le future, j’aimerais créer un script qui automatiquement build et upload la nouvelle version du site sur mon serveur dès qu'un nouveau post ou une nouvelle publication est détecté.


---



### *Bibliography:*

[58 bytes of css to look great nearly everywhere · GitHub](https://gist.github.com/JoeyBurzynski/617fb6201335779f8424ad9528b72c41)

[Pontoporeia/tinyblog - tinyblog - Codeberg.org](https://codeberg.org/Pontoporeia/tinyblog)

[100 Bytes of CSS to look great everywhere](https://www.swyx.io/css-100-bytes)



[Pontoporeia/blog-zola - blog-zola - Codeberg.org](https://codeberg.org/Pontoporeia/blog-zola)

[https://solar.lowtechmagazine.com](https://solar.lowtechmagazine.com)